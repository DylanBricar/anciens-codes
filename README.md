# Accès aux anciens codes

Afin de décharger le serveur de l'ESI de tous les dépôts qui avaient été créés et puisque plusieurs personnes semblaient utiliser les codes qui étaient ici, tout a été migré vers GitHub.

Voici le lien : https://github.com/DylanBricar?tab=repositories

**RAPPEL** : inutile de copier/coller des codes si vous ne les comprenez pas (et je n'ai pas le niveau d'un prof) :) !

Bon courage !
